
default: sim

sim:
	make -C ./example_design/sim all

verdi:
	make -C ./example_design/verdi


clean:
	rm -rf .*~
	make -C ./example_design/sim clean
	make -C ./example_design/verdi clean
