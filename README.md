# run mig\_axi\_ddr3 simulation/debug with vcs/verdi

1. project is based on example design of mig\_axi\_ddr3 generated by Xilinx Vivado
2. just modify vcs\_run.sh to make all stuff ready for command.

# usage:
1. clone this repo.
2. cd mig\_axi\_ddr3
3. modify "XILINX\_VIVADO" definition inside ./example\_design/sim/vcs\_run.sh  
4. make sim
5. make verdi, then open sim.fsdb which should inside ./exmaple\_design/sim
