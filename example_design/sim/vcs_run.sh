#!/bin/bash
###############################################################################

#echo Simulation Tool: VCS
export XILINX_VIVADO=/mnt/d/Xilinx/Vivado/2019.2 

VXX_OPTS="-full64 -LDFLAGS -Wl,-no-as-needed"
VCS_OPTS="-timescale=1ns/10ps -debug_access+all -lca -kdb -fsdb"

#Compile the required libraries here#
XILIB_DIR="$XILINX_VIVADO/data/verilog/src +libext+.v"

if [[ "$1" == "xilib" || -z "$1" ]]; then
	vlogan -sverilog +incdir+$XLIB_DIR $XILINX_VIVADO/data/verilog/src/unisims/*.v -l vcs_xilib.log
	vlogan -sverilog +incdir+$XLIB_DIR $XILINX_VIVADO/data/verilog/src/unimacro/*.v -l vcs_xilib.log
	vlogan -sverilog +incdir+$XLIB_DIR $XILINX_VIVADO/data/verilog/src/retarget/*.v -l vcs_xilib.log
	vlogan -sverilog +incdir+$XLIB_DIR -f $XILINX_VIVADO/data/secureip/secureip_cell.list.f -l vcs_xilib.log
fi
#libraries path#

[[ "$1" == "xilib" ]] && exit 0

#Compile all modules#
if [[ "$1" == "mig" || -z "$1" ]]; then
	vlogan -sverilog ../../user_design/rtl/mig_axi_ddr3.v -l vcs_elab.log
	vlogan -sverilog ../../user_design/rtl/mig_axi_ddr3_mig_sim.v -l vcs_elab.log
	vlogan -sverilog ../rtl/traffic_gen/mig_7series*.v -l vcs_elab.log
	vlogan -sverilog ../../user_design/rtl/clocking/*.v -l vcs_elab.log
	vlogan -sverilog ../../user_design/rtl/controller/*.v -l vcs_elab.log
	vlogan -sverilog ../../user_design/rtl/ecc/*.v -l vcs_elab.log
	vlogan -sverilog ../../user_design/rtl/ip_top/*.v -l vcs_elab.log
	vlogan -sverilog ../../user_design/rtl/phy/*.v -l vcs_elab.log
	vlogan -sverilog ../../user_design/rtl/ui/*.v -l vcs_elab.log
	vlogan -sverilog ../../user_design/rtl/ui/*.v -l  vcs_elab.log
	vlogan -sverilog ../../user_design/rtl/axi/*.v -l vcs_elab.log
fi

[[ "$1" == "mig" ]] && exit 0

if [[ "$1" == "run" || -z "$1" ]]; then
	vlogan -sverilog ../rtl/example_top.v -l vcs_elab.log

	#Compile files in sim folder (excluding model parameter file)#
	#$XILINX variable must be set
	vlogan $XILINX_VIVADO/data/verilog/src/glbl.v -l vcs_elab.log
	vlogan -sverilog wiredly.v -l vcs_elab.log
	vlogan -sverilog sim_tb_top.v -l vcs_elab.log

	#Pass the parameters for memory model parameter file#
	vlogan -Xcheck_p1800_2009=char -sverilog +define+x4Gb +define+sg107E +define+x16 ddr3_model.sv -l vcs_elab.log 

	#Simulate the design with sim_tb_top as the top module
	vcs $VXX_OPTS $VCS_OPTS sim_tb_top glbl -l vcs_run.log

	#Run sim
	./simv 
fi
	#echo done
